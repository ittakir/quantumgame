#include <Windows.h> //needed for gl.h
#include <gl/gl.h>
#include <gl/glu.h>
#include <math.h>

#include "CGLRender.h"
#include "CScene.h"
#include "CEnemyBall.h"


#define PI 3.14159265


CGLRender::CGLRender()
    : CRender()
    , m_aspect(1.0)
{
}


void CGLRender::init()
{
    //Initialize OpenGL settings
    glShadeModel(GL_SMOOTH);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClearDepth(1.0f);
    glDisable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    buildFont();
}


void CGLRender::close()
{
    killFont();
}


void CGLRender::resizeWindow(int _width, int _height)
{
    //Update output GL viewport when application window resizes
    if(_height <= 0)
	_height = 1;
    if(_width <= 0)
	_width = 1;

    CRender::resizeWindow(_width, _height);

    m_aspect = double(_height) / double(_width);

    glViewport(0, 0, _width, _height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, SceneMaxX, SceneMaxX * m_aspect, 0, -1, 1);
    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity();
}


void CGLRender::drawScene(const CScene& _scene, int _score)
{
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();

    for(TEnemyList::const_iterator it = _scene.enemies().begin(); it != _scene.enemies().end(); ++it) {
	//Find enemy type and draw it
	switch((*it)->type()) {
	case(CEnemy::Ball): {
	    CEnemyBall* e = static_cast<CEnemyBall*>(*it);

	    glColor4ubv(e->color().vec());

	    glBegin(GL_TRIANGLE_FAN);
	    double cx = e->center().x;
	    double cy = e->center().y;
	    double r = e->radius();

	    glVertex2d(cx, cy);
	    for(int i = 0; i <= 360; i += 360 / 20) {
		double angle = i * PI / 180.0;
		glVertex2d(cx + sin(angle) * r, cy + cos(angle) * r);
	    }
	    glEnd();


	    glColor3ub(127, 127, 127);
	    glBegin(GL_LINE_LOOP);
	    for(int i = 0; i <= 360; i += 360 / 20) {
		double angle = i * PI / 180.0;
		glVertex2d(cx + sin(angle) * r, cy + cos(angle) * r);
	    }
	    glEnd();

	    break;
	}
	}
    } //for

    //Draw current score
    glColor3ub(255, 255, 255);
    glRasterPos2d(1, SceneMaxX * m_aspect - 1);
    printString("%d", _score);

    //Finish drawing and wait OpenGL commands to finish
    glFinish();
}


CPointF CGLRender::windowToSceneCoordinates(const CPoint& _pos) const
{
    CPointF res;
    res.x = double(_pos.x) * SceneMaxX / double(m_windowSize.x);
    res.y = double(_pos.y) * SceneMaxX * m_aspect / double(m_windowSize.y);
    return res;
}


//-----------------------------------------------------------------------------
void CGLRender::buildFont()
{
    m_fontBase = glGenLists(96);

    HFONT font = CreateFont(-32, 0, 0, 0, FW_BOLD, FALSE, FALSE, FALSE,
	    ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, ANTIALIASED_QUALITY,
	    FF_DONTCARE | DEFAULT_PITCH, L"Comic Sans Ms");

    HDC hdc = theApp.hdc();
    HFONT oldfont = (HFONT)SelectObject(hdc, font);
    wglUseFontBitmaps(hdc, 32, 96, m_fontBase); //Builds 96 characters starting at character 32
    SelectObject(hdc, oldfont);
    DeleteObject(font);
}


void CGLRender::killFont()
{
    glDeleteLists(m_fontBase, 96);
}


void CGLRender::printString(const char* _fmt, ...)
{
    if(_fmt == NULL)
	return;

    //Prepare text
    char text[256];
    va_list ap; //Pointer to list of arguments
    va_start(ap, _fmt); //Parse the string for variables
    vsprintf_s(text, 256, _fmt, ap); //...and convert symbols to actual numbers
    va_end(ap);

    //Draw text with display lists
    glPushAttrib(GL_LIST_BIT);
    glListBase(m_fontBase - 32);
    glCallLists(strlen(text), GL_UNSIGNED_BYTE, text); //Draw the display list text
    glPopAttrib();
}
