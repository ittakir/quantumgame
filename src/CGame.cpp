#include "CGame.h"
#include "CApplication.h"
#include "CEvent.h"
#include "CEnemy.h"
#include "CScene.h"
#include "CGLRender.h"

#include <Windows.h>


CGame::CGame(CGLRender* _render)
    : m_scene(NULL)
    , m_render(_render)
{
    startNewGame();
}


CGame::~CGame()
{
    if(m_scene)
	delete m_scene;
}

void CGame::startNewGame()
{
    m_scene = new CScene();
    m_score = 0;
    srand((unsigned int)m_stopWatch.start());
}


bool CGame::run()
{
    return process();
}


bool CGame::process()
{
    if(!m_scene || !m_render)
	return false;

    if(m_stopWatch.elapsed() < 5.0) //Don't update game too often
	return false;

    //Get and process all mouse events from CApplication
    CEvent* event;
    while(event = theApp.nextEvent()) {
	CMousePressEvent* mouseEvent = dynamic_cast<CMousePressEvent*>(event);
	if(mouseEvent && mouseEvent->isLeftPressed()) {
	    //Left click detected, find scene coordinates.
	    CPointF p = m_render->windowToSceneCoordinates(mouseEvent->pos());
	    //Find enemy at click coordinates
	    CEnemy* enemy = m_scene->findEnemy(p);
	    if(enemy) {
		//We found enemy, kill it
		m_score += enemy->score();
		m_scene->removeEnemy(enemy);
	    }
	}
    }

    //Calculate elapsed time and restart timer
    const double dt = m_stopWatch.start();

    //Create new enemy periodically
    static double elapsedTime = 0;
    elapsedTime += dt;
    if(elapsedTime > 500.0) {
	elapsedTime = 0;
	m_scene->addEnemy();
    }

    //Process movement of scene object
    m_scene->process(dt, m_render->windowSize());
    //Draw scene with OpenGL render
    m_render->drawScene(*m_scene, m_score);

    return true; //We have drawed something
}
