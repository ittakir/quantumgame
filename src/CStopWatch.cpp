#include <Windows.h>

#include "CStopWatch.h"


CStopWatch::CStopWatch()
{
    LARGE_INTEGER freq;
    if(QueryPerformanceFrequency(&freq)) {
	m_highPrecision = true;
	m_freq = (double)freq.QuadPart;
    } else
	m_highPrecision = false;
    m_lastTime = 0;
}


double CStopWatch::start()
{
    double res;
    if(m_highPrecision) {
	LARGE_INTEGER cnt;
	QueryPerformanceCounter(&cnt);
	res = ((cnt.QuadPart - m_lastTime) * 1000ll) / m_freq;
	m_lastTime = cnt.QuadPart;
    } else {
	unsigned int current = GetTickCount();
	unsigned int last = (unsigned int)m_lastTime;
	res = (current - last);
	m_lastTime = current;
    }
    return res;
}


double CStopWatch::elapsed() const
{
    double res;
    if(m_highPrecision) {
	LARGE_INTEGER cnt;
	QueryPerformanceCounter(&cnt);
	res = ((cnt.QuadPart - m_lastTime) * 1000ll) / m_freq;
    } else {
	unsigned int current = GetTickCount();
	unsigned int last = (unsigned int)m_lastTime;
	res = (current - last);
    }
    return res;
}
