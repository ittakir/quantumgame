#ifndef CGAME_H
#define CGAME_H


#include "CApplication.h"
#include "CStopWatch.h"

class CScene;
class CGLRender;


//Handles game logic. How we should kill enemy, calculate scores, etc.
class CGame : public CCallback
{
public:
    CGame(CGLRender* _render);
    ~CGame();
    void init();
    void startNewGame();
    void close();
    bool run(); //Callback, which periodically called from CApplication.

protected:
    //Processes one game tick in terms of gameplay.
    bool process();

private:
    void showMenu();
    void showScene();

    CScene* m_scene; //Scene with geometry objects
    int m_score;     //Game score
    CStopWatch m_stopWatch;
    CGLRender* m_render;
};


#endif // CGAME_H
