#ifndef CEVENT_H
#define CEVENT_H


#include "Common.h"


//General event interface
class CEvent
{
public:
    CEvent()
    {}

    virtual ~CEvent()
    {}
};


//Mouse button press event
class CMousePressEvent : public CEvent
{
public:
    enum {
	ButtonLeft = 1,
	ButtonRight = 2
    };

    CMousePressEvent(const CPoint& _pos, int _buttons)
	: m_pos(_pos)
	, m_buttons(_buttons)
    {}

    bool isLeftPressed() const { return ((m_buttons & ButtonLeft) != 0); }
    bool isRightPressed() const { return ((m_buttons & ButtonRight) != 0); }
    CPoint pos() const { return m_pos; }

private:
    CPoint m_pos;
    int m_buttons;
};


#endif // CEVENT_H
