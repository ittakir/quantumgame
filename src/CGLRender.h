#ifndef CGLRENDER_H
#define CGLRENDER_H


#include "CRender.h"
#include "Common.h"

class CScene;


//OpenGL renderer.
class CGLRender : public CRender
{
public:
    CGLRender();

    void init();
    void close();

    void resizeWindow(int _width, int _height);

    //Draw game scene
    void drawScene(const CScene &_scene, int _score);

    //Convert window coordinates in pixels to scene coordinates
    CPointF windowToSceneCoordinates(const CPoint& _pos) const;

private:
    //Prepare font
    void buildFont();
    //Destroy font
    void killFont();
    //Print text on screen
    void printString(const char *_fmt, ...);

private:
    double m_aspect;
    int m_fontBase;
};


#endif // CGLRENDER_H
