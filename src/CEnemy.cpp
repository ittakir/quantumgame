#include "CEnemy.h"


CEnemy::CEnemy(const CPointF& _center, double _radius)
    : m_center(_center)
    , m_radius(_radius)
    , m_speed(0, 0)
{
}
