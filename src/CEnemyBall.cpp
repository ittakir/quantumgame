#include "CEnemyBall.h"


CEnemyBall::CEnemyBall(const CPointF &_center, double _radius)
    : CEnemy(_center, _radius)
{
}


int CEnemyBall::score() const
{
    //Score depends on speed and radius
    return int(speed().y / radius() + 0.5);
}


bool CEnemyBall::intersects(const CPointF& _point) const
{
    //Distance between center and point must be less than radius
    return (m_center.distance(_point) < m_radius);
}
