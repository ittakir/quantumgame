#ifndef CSTOPWATCH_H
#define CSTOPWATCH_H


//Class to measuretime intervals
//If high precision timer is not available, then it uses old WinApi func.
class CStopWatch
{
public:
    CStopWatch();

    //Restart stopwatch and return elapsed time since last start() call
    //Time interval is in milliseconds.
    double start();
    //Get elapsed time without timer restart.
    double elapsed() const;

private:
    bool m_highPrecision;
    long long m_lastTime;
    double m_freq;
};


#endif // CSTOPWATCH_H
