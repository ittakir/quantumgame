#ifndef CENEMYBALL_H
#define CENEMYBALL_H


#include "CEnemy.h"


//Colorized ball enemy
class CEnemyBall : public CEnemy
{
public:
    CEnemyBall(const CPointF& _center, double _radius);

    EnemyType type() const { return Ball; }

    CColor color() const { return m_color; }
    void setColor(const CColor& _color) { m_color = _color; }
    int score() const;
    virtual bool intersects(const CPointF& _point) const;

private:
    CColor m_color;
};


#endif // CENEMYBALL_H
