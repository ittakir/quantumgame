#ifndef CAPPLICATION_H
#define CAPPLICATION_H

#include <windows.h>
#include <string>
#include <queue>

class CEvent;
class CRender;


#define theApp CApplication::instance()


//Interface for periodic calls from CApplication
class CCallback
{
public:
    CCallback()
    {}

    virtual bool run() = 0;
};


//-----------------------------------------------------------------------------
//Windows application facility.
//Singleton.
class CApplication
{
public:
    static CApplication& instance() {
	static CApplication theInstance;
	return theInstance;
    }
    ~CApplication();

    //Create application window
    bool createWindow(const wchar_t* _title, int _width, int _height, int _bits, bool _isFullscreen);

    //Assign render object
    void setRender(CRender* _render);

    //Run message loop
    int exec(CCallback* _cb);

    //Get next available event
    CEvent* nextEvent();

    //Process messages from windows message loop
    bool processMessage(UINT _uMsg, WPARAM _wParam, LPARAM _lParam);
    HDC hdc() const { return m_hdc; }
    std::wstring lastError() const;

private:
    //Close application window
    void closeWindow();

private:
    CApplication();
    CApplication(const CApplication& _app);
    CApplication& operator=(const CApplication&);

private:
    HDC m_hdc;   // Private GDI Device Context
    HGLRC m_hrc; // Permanent Rendering Context
    HWND m_hwnd; // Holds Our Window Handle
    HINSTANCE m_instance; // Holds The Instance Of The Application
    bool m_fullscreen;

    bool m_keys[256]; // Array Used For The Keyboard Routine
    bool m_active; // Window Active Flag Set To TRUE By Default

    std::wstring m_lastError;
    std::queue<CEvent*> m_messageQueue;

    CRender* m_render;
};


#endif // CAPPLICATION_H
