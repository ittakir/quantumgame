#ifndef COMMON_H
#define COMMON_H


#include <math.h>


//Scene width from left to right is constant value SceneMaxX, independent of window size.
//Height of scene is calculated from SceneMaxX and aspect ration of the window.
static const double SceneMaxX = 100.0;


//2D point with real coordinates
class CPointF
{
public:
    CPointF()
    {}

    CPointF(double _x, double _y)
	: x(_x)
	, y(_y)
    {}

    double distance(const CPointF& _point) const
    {
	double dx = _point.x - x;
	double dy = _point.y - y;
	return sqrt(dx*dx + dy*dy);
    }

public:
    double x;
    double y;
};


//2D point with integer coordinates
class CPoint
{
public:
    CPoint()
    {}

    CPoint(int _x, int _y)
	: x(_x)
	, y(_y)
    {}

public:
    int x;
    int y;
};


//Color with 4 components
class CColor
{
public:
    CColor()
    {
	m_col[0] = 0;
	m_col[1] = 0;
	m_col[2] = 0;
	m_col[3] = 0xFF;
    }

    CColor(unsigned char _red, unsigned char _green, unsigned char _blue, unsigned char _alpha = 0xFF)
    {
	m_col[0] = _red;
	m_col[1] = _green;
	m_col[2] = _blue;
	m_col[3] = _alpha;
    }

    unsigned char r() const { return m_col[0]; }
    unsigned char g() const { return m_col[1]; }
    unsigned char b() const { return m_col[2]; }
    unsigned char a() const { return m_col[3]; }

    const unsigned char* vec() const { return m_col; }

private:
    unsigned char m_col[4];
};


#endif // COMMON_H
