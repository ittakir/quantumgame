#include "CApplication.h"
#include "CGLRender.h"
#include "CGame.h"


int WINAPI WinMain(HINSTANCE /*_instance*/, HINSTANCE /*_prevInstance*/,
	LPSTR /*_cmdLine*/, int /*_cmdShow*/)
{
    CGLRender render;
    CGame game(&render);

    //Create application window
    if (!theApp.createWindow(L"QuantumGame", 640, 480, 32, false)) {
	MessageBox(NULL, theApp.lastError().c_str(), L"Error", MB_OK | MB_ICONEXCLAMATION);
	return 1;
    }

    //Run message loop
    return theApp.exec(&game);
}
