#include "CApplication.h"
#include "CEvent.h"
#include "CRender.h"

#include <WindowsX.h>


static wchar_t* ClassName = L"OpenGL";


LRESULT CALLBACK WndProc(HWND _hWnd, UINT _uMsg, WPARAM _wParam, LPARAM _lParam)
{
    if (theApp.processMessage(_uMsg, _wParam, _lParam))
	return 0;
    //Pass all unhandled messages to DefWindowProc
    return ::DefWindowProc(_hWnd, _uMsg, _wParam, _lParam);
}


CApplication::CApplication()
    : m_hdc(NULL)
    , m_hrc(NULL)
    , m_hwnd(NULL)
    , m_fullscreen(false)
    , m_active(true)
    , m_render(NULL)
{}


CApplication::~CApplication()
{
    //Clean message queue
    while(!m_messageQueue.empty()) {
	delete m_messageQueue.front();
	m_messageQueue.pop();
    }
}


bool CApplication::createWindow(const wchar_t* _title, int _width, int _height, int _bits, bool _isFullscreen)
{
    m_fullscreen = _isFullscreen;
    m_instance = GetModuleHandle(NULL); //Grab An Instance For Our Window

    WNDCLASS wc;
    wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; // Redraw On Size, And Own DC For Window.
    wc.lpfnWndProc = (WNDPROC) WndProc; // WndProc Handles Messages
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = m_instance;
    wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = NULL; // No Background Required For GL
    wc.lpszMenuName = NULL; // We Don't Want A Menu
    wc.lpszClassName = ClassName; // Set The Class Name

    if (!RegisterClass(&wc)) {
	m_lastError = L"Failed to register the window class";
	return false;
    }

    if (m_fullscreen) {
	DEVMODE devMode;
	memset(&devMode, 0, sizeof(devMode));
	devMode.dmSize = sizeof(devMode);
	devMode.dmPelsWidth = _width;
	devMode.dmPelsHeight = _height;
	devMode.dmBitsPerPel = _bits;
	devMode.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

	//Try to set selected mode
	if (ChangeDisplaySettings(&devMode, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL) {
	    //if fullscreen mode fails, try to use windowed mode
	    m_fullscreen = false;
	}
    }

    DWORD dwExStyle;
    DWORD dwStyle;

    if (m_fullscreen) {
	dwExStyle = WS_EX_APPWINDOW;
	dwStyle = WS_POPUP;
	ShowCursor(FALSE);
    } else {
	dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
	dwStyle = WS_OVERLAPPEDWINDOW;
    }

    RECT rect;
    rect.left = 0;
    rect.right = _width;
    rect.top = 0;
    rect.bottom = _height;

    AdjustWindowRectEx(&rect, dwStyle, FALSE, dwExStyle);

    //Create the window
    m_hwnd = CreateWindowEx(dwExStyle, ClassName, _title, dwStyle | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
	    0, 0, rect.right-rect.left, rect.bottom-rect.top,
	    NULL, NULL, m_instance, NULL);
    if (!m_hwnd) {
	closeWindow();
	m_lastError = L"Window creation error";
	return false;
    }

    static PIXELFORMATDESCRIPTOR pfd = {
	    sizeof(PIXELFORMATDESCRIPTOR), // Size Of This Pixel Format Descriptor
	    1,                             // Version Number
	    PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,
	    PFD_TYPE_RGBA,
	    _bits,
	    0, 0, 0, 0, 0, 0, // Color Bits Ignored
	    0, // No Alpha Buffer
	    0, // Shift Bit Ignored
	    0, // No Accumulation Buffer
	    0, 0, 0, 0, // Accumulation Bits Ignored
	    16, // 16Bit Z-Buffer (Depth Buffer)
	    0, // No Stencil Buffer
	    0, // No Auxiliary Buffer
	    PFD_MAIN_PLANE, // Main Drawing Layer
	    0, // Reserved
	    0, 0, 0 // Layer Masks Ignored
    };

    m_hdc=GetDC(m_hwnd);
    if (!m_hdc) {
	closeWindow();
	m_lastError = L"Can't create a GL device context";
	return false;
    }

    int pixelFormat = ChoosePixelFormat(m_hdc, &pfd);
    if (!pixelFormat) {
	closeWindow();
	m_lastError = L"Can't find a suitable PixelFormat";
	return false;
    }

    if (!SetPixelFormat(m_hdc, pixelFormat, &pfd)) {
	closeWindow();
	m_lastError = L"Can't set the PixelFormat";
	return false;
    }

    m_hrc=wglCreateContext(m_hdc);
    if (!m_hrc) {
	closeWindow();
	m_lastError = L"Can't create a GL rendering context";
	return false;
    }

    if(!wglMakeCurrent(m_hdc, m_hrc)) {
	closeWindow();
	m_lastError = L"Can't activate the GL rendering context";
	return false;
    }

    ShowWindow(m_hwnd, SW_SHOW);
    SetForegroundWindow(m_hwnd);
    SetFocus(m_hwnd);

    if(m_render) {
	m_render->init();
	m_render->resizeWindow(_width, _height);
    }

    return true;
}


void CApplication::closeWindow()
{
    if(m_render)
	m_render->close();

    if (m_fullscreen) {
	ChangeDisplaySettings(NULL, 0);
	ShowCursor(TRUE);
    }

    if (m_hrc) {
	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(m_hrc);
	m_hrc = NULL;
    }

    if (m_hdc && !ReleaseDC(m_hwnd, m_hdc))
	m_hdc = NULL;

    if (m_hwnd && !DestroyWindow(m_hwnd))
	m_hwnd = NULL;

    if (!UnregisterClass(ClassName, m_instance))
	m_instance = NULL;
}


int CApplication::exec(CCallback *_cb)
{
    MSG msg;
    bool done = false;
    while (!done) {
	if (::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
	    if (msg.message == WM_QUIT) {
		done = true;
	    } else {
		::TranslateMessage(&msg);
		::DispatchMessage(&msg);
	    }
	} else {
	    // Draw The Scene.  Watch For ESC Key And Quit Messages From DrawGLScene()
	    if (m_active) {
		if (m_keys[VK_ESCAPE]) {
		    done = true;
		} else {
		    if(_cb) {
			if(_cb->run())
			    ::SwapBuffers(m_hdc);
		    }
		    Sleep(0);
		}
	    }
	}
    }

    //Shutdown
    closeWindow();
    return msg.wParam;
}


CEvent* CApplication::nextEvent()
{
    if(m_messageQueue.empty())
	return NULL;
    CEvent* res = m_messageQueue.front();
    m_messageQueue.pop();
    return res;
}


void CApplication::setRender(CRender* _render)
{
    m_render = _render;
}


bool CApplication::processMessage(UINT _uMsg, WPARAM _wParam, LPARAM _lParam)
{
    switch (_uMsg) {
    case WM_ACTIVATE: { // Watch For Window Activate Message
	    if (!HIWORD(_wParam)) { // Check Minimization State
		m_active = true; // Program Is Active
	    } else {
		m_active = false; // Program Is No Longer Active
	    }
	    return true; // Return To The Message Loop
	}

    case WM_SYSCOMMAND: { // Intercept System Commands
	if (_wParam == SC_SCREENSAVE || _wParam == SC_MONITORPOWER) // Check System Calls
	    return true; // Prevent From Happening
	break;
    }

    case WM_CLOSE: //Close Message
	::PostQuitMessage(0); // Send A Quit Message
	return true;

    case WM_LBUTTONDOWN: {
	//Add mouse event
	CPoint pos(GET_X_LPARAM(_lParam), GET_Y_LPARAM(_lParam));
	m_messageQueue.push(new CMousePressEvent(pos, CMousePressEvent::ButtonLeft));
	while(m_messageQueue.size() > 100) {
	    delete m_messageQueue.front();
	    m_messageQueue.pop();
	}
	return true;
    }

    case WM_KEYDOWN: // Is A Key Being Held Down?
	m_keys[_wParam] = true;
	return true;

    case WM_KEYUP:
	m_keys[_wParam] = false;
	return true;

    case WM_SIZE: //Resize The OpenGL Window
	if(m_render)
	    m_render->resizeWindow(LOWORD(_lParam), HIWORD(_lParam));
	return true;
    }
    return false;
}


std::wstring CApplication::lastError() const
{
    return m_lastError;
}
