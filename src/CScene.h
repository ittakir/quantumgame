#ifndef CSCENE_H
#define CSCENE_H


#include <list>

#include "Common.h"

class CEnemy;

typedef std::list<CEnemy*> TEnemyList;


//Handles all objects in the game scene.
//Full window space has coordinates [0..100][0..100*aspect]
class CScene
{
public:
    CScene();
    ~CScene();

    //Create new random enemy and add it to scene
    void addEnemy();
    //Remove enemy from scene
    void removeEnemy(CEnemy *_enemy);
    //Find enemy at scene coordinates. Smaller enemies have more priority.
    CEnemy* findEnemy(const CPointF& _pos) const;
    //Update scene objects positions
    void process(double _dt, const CPoint &_windowSize);
    const TEnemyList& enemies() const { return m_enemies; }

private:
    TEnemyList m_enemies; //Elements are sorted by decreasing radius.
};


#endif // CSCENE_H
