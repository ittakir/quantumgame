#ifndef CRENDER_H
#define CRENDER_H


#include "CApplication.h"
#include "Common.h"


//Base abstract class for renderer, used to connect specific renderer with application.
class CRender
{
public:
    CRender()
    {
	//Register render in appication
	theApp.setRender(this);
    }

    virtual ~CRender()
    {
	//Unregister render
	theApp.setRender(NULL);
    }

    //Initialization
    virtual void init() = 0;
    //Finalization
    virtual void close() = 0;
    //Window resized
    virtual void resizeWindow(int _width, int _height)
    {
	m_windowSize = CPoint(_width, _height);
    }

    virtual CPoint windowSize() const { return m_windowSize; }

protected:
    CPoint m_windowSize;
};


#endif // CRENDER_H
