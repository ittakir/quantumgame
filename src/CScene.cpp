#include "CScene.h"
#include "CEnemyBall.h"


//Size of enemy in scene coordinates
static const double MaxBallRadius = 10.0;
static const double MinBallRadius = 2.0;

//Speed of enemy in scene coordinates per second.
static const double MaxBallSpeed = 35.0;
static const double MinBallSpeed = 15.0;

//Get random in [0..1.0]
#define randf() (double(rand()) / double(RAND_MAX))


CScene::CScene()
{
}


CScene::~CScene()
{
    for(TEnemyList::iterator it = m_enemies.begin(); it != m_enemies.end(); ++it)
	delete *it;
}


//-----------------------------------------------------------------------------
//Place where enemy should be created, right upper the scene top.
static CPointF randomStartPoint()
{
    double r = MinBallRadius + (MaxBallRadius - MinBallRadius) * randf();
    CPointF p;
    p.x = r + (SceneMaxX - 2*r) * randf();
    p.y = -r;
    return p;
}


static CColor randomColor()
{
    return CColor(rand() & 0xFF, rand() & 0xFF, rand() & 0xFF);
}


static double speedForRadius(double _r)
{
    double k = (_r - MinBallRadius) / (MaxBallRadius - MinBallRadius);
    return MaxBallSpeed - (MaxBallSpeed - MinBallSpeed) * k;
}


//-----------------------------------------------------------------------------
void CScene::addEnemy()
{
    //Create new random enemy ball
    CPointF center = ::randomStartPoint();
    double r = -center.y;

    CEnemyBall* e = new CEnemyBall(center, r);

    e->setColor(::randomColor());
    e->setSpeed(CPointF(0, ::speedForRadius(r))); //Set only vertical speed

    //Insert enemy in order of increasing radius
    for(TEnemyList::iterator it = m_enemies.begin(); ; ++it) {
	if(it != m_enemies.end()) {
	    if(r > (*it)->radius()) {
		m_enemies.insert(it, e); //Insert right before first big element
		break;
	    }
	} else {
	    //All elements were bigger, add to the end of list.
	    m_enemies.push_back(e);
	    break;
	}
    }
}


void CScene::removeEnemy(CEnemy* _enemy)
{
    //Remove enemy from list and delete it
    m_enemies.remove(_enemy);
    delete _enemy;
}


CEnemy* CScene::findEnemy(const CPointF& _pos) const
{
    for(TEnemyList::const_reverse_iterator it = m_enemies.rbegin(); it != m_enemies.rend(); ++it) {
	if((*it)->intersects(_pos)) //Ask enemy if it has this point
	    return *it;
    }
    return NULL; //Nothing was found
}


//Processes one game tick in terms of geometry objects.
void CScene::process(double _dt, const CPoint& _windowSize)
{
    //Prevent jump on lags
    if(_dt > 500.0)
	_dt = 500.0;

    for(TEnemyList::iterator it = m_enemies.begin(); it != m_enemies.end();) {
	//Move each enemy and test if it goes out of scene
	CEnemy* e = *it;
	CPointF center = e->center();
	center.x += e->speed().x * _dt / 1000.0;
	center.y += e->speed().y * _dt / 1000.0;
	e->setCenter(center);

	//Calculate scene height
	double maxY = double(_windowSize.y) * SceneMaxX / double(_windowSize.x);

	//Test if enemy goes out of scene
	if((center.y - e->radius()) > maxY) {
	    it = m_enemies.erase(it);
	    delete e;
	} else
	    ++it;
    }
}
