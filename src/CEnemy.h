#ifndef CENEMY_H
#define CENEMY_H


#include "Common.h"


//General enemy class
class CEnemy
{
public:
    enum EnemyType
    {
	Unknown,
	Ball
    };

public:
    CEnemy(const CPointF& _center, double _radius);

    virtual EnemyType type() const = 0;

    CPointF center() const { return m_center; }
    void setCenter(const CPointF& _center) { m_center = _center; }

    //This is BBox for enemy. In our game with round enemies, it's a bounding circle.
    double radius() const { return m_radius; }

    CPointF speed() const { return m_speed; }
    void setSpeed(const CPointF& _speed) { m_speed = _speed; }

    virtual int score() const { return 0; }
    virtual bool intersects(const CPointF& _point) const = 0;

protected:
    CPointF m_center;
    CPointF m_speed;
    double m_radius;
};


#endif // CENEMY_H
