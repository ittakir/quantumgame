TEMPLATE = app
CONFIG -= console
CONFIG -= app_bundle
CONFIG -= qt


LIBS += opengl32.lib glu32.lib user32.lib gdi32.lib

SOURCES += \
    src/main.cpp \
    src/CApplication.cpp \
    src/CGame.cpp \
    src/CScene.cpp \
    src/CEnemy.cpp \
    src/CEnemyBall.cpp \
    src/CGLRender.cpp \
    src/CStopWatch.cpp

HEADERS += \
    src/CApplication.h \
    src/CRender.h \
    src/CGame.h \
    src/CScene.h \
    src/CEnemy.h \
    src/CEnemyBall.h \
    src/Common.h \
    src/CEvent.h \
    src/CGLRender.h \
    src/CStopWatch.h
